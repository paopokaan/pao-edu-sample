import { Component, OnInit } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';

import { PagerService } from './../../services/pager.service';

import * as _ from 'underscore';

@Component({
    selector: 'app-sample-paging',
    templateUrl: './sample-paging.component.html',
    styleUrls: ['./sample-paging.component.css']
})
export class SamplePagingComponent implements OnInit {

    constructor(private http: Http, private pagerService: PagerService) { }

    // array of all items to be paged
    private allItems: any[];

    // pager object
    pager: any = {};

    // paged items
    pagedItems: any[];

    ngOnInit() {
    }


}
