import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { NgForm } from '@angular/forms';
import { Observable } from 'rxjs/Observable';

import { Content } from './../../models/content.model';

import { SaveStudentService } from './../../services/save-student.service';
import { ProgressIndicatorService } from './../../services/progress-indicator.service';

@Component({
  selector: 'app-first',
  templateUrl: './first.component.html',
  styleUrls: ['./first.component.css']
})
export class FirstComponent implements OnInit {

  listContent: Content[];
  log(val) { console.log(val); }

  constructor(
    private saveStudentService: SaveStudentService,
    private router: Router,
    private progress: ProgressIndicatorService) { }

  private handleError(errors: any): void {
    console.log('Terjadi error : ' + errors);
  }

  ngOnInit() {
    // this.getStudent();
    this.getStudent2();
  }

  getStudent() {
    this.saveStudentService.getStudent()
      .then(result => this.listContent = result)
      .catch(this.handleError);
  }

  getStudent2() {
    this.saveStudentService.getStudent2()
      .subscribe(result => this.listContent = result,
          err => {
            console.log(err);
      });
  }

  //////save data/////////////////
  student = new Content(undefined, undefined, 0, undefined);

  loadEditData(s: Content): void {
    this.student = new Content(s.id, s.name, s.year, s.address);
  }

  checkConsole() {
    console.log('test');
  }

  saveOrUpdateStudent2(s: Content) {
    this.progress.toggleIndicator('process data');
    let operation: Observable<any>;
    if (s.id) {
      console.log('update ' + s.id);
      operation = this.saveStudentService.updateStudent2(s, s.id);
      operation.subscribe(
        () => {
          this.progress.toggleIndicator(null);
          this.getStudent2();
        }
      );
    } else {
      console.log('save new data');
      operation = this.saveStudentService.saveStudent2(s);
      operation
        .subscribe(
        () => {
          this.progress.toggleIndicator(null);
          this.getStudent2();
        }
        );
    }
  }

  saveOrUpdateStudent3(student: Content) {
    this.progress.toggleIndicator('process data');
    console.log('save new data');
    this.saveStudentService.saveStudent3(student);
    this.progress.toggleIndicator(null);
  }

  saveOrUpdateStudent(s: Content): void {
    this.progress.toggleIndicator('process update data');

    if (s.id) {
      this.saveStudentService.updateStudent(s, s.id)
        .then(() => {
          this.progress.toggleIndicator(null);
          this.listContent;
        })
        .catch(error => {
          console.log('failed save data student ' + error);
          this.progress.toggleIndicator(null);
        });

    } else {

      this.saveStudentService.saveStudent(s)
        .then(() => {
          this.progress.toggleIndicator(null);
          this.listContent;
        })
        .catch(error => {
          console.log('failed save data student ' + error);
          this.progress.toggleIndicator(null);
        });
    }
  }

  deleteStudent(s: Content): void {
    this.progress.toggleIndicator('process to delete');
    this.saveStudentService.deleteStudent(s.id)
      .then(() => {
        this.deleteNotification();
        this.progress.toggleIndicator(null);
      })
      .catch(error => {
        console.log('failed delete data ' + error);
        this.progress.toggleIndicator(null);
      });
    this.listContent;
  }

  deleteStudent2(id: string) {
    console.log('delele id: ' + id);
    this.saveStudentService.deleteStudent2(id)
      .subscribe(
      () => {
        this.progress.toggleIndicator(null);
        this.deleteNotification();
      },
      err => { console.log(err); }
      );
  }

  deleteNotification() {
    this.progress.toggleIndicator('Data has been deleted');
    setTimeout(() => this.progress.toggleIndicator(null), 3000);
    this.getStudent2();
  }

  get debugForm() {
    return JSON.stringify(this.student);
  }

}
