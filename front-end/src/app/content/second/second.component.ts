import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Router }   from '@angular/router';

import { Content } from './../../models/content.model';
import { SaveStudentService } from './../../services/save-student.service';
import { ProgressIndicatorService } from './../../services/progress-indicator.service';

@Component({
  selector: 'app-second',
  templateUrl: './second.component.html',
  styleUrls: ['./second.component.css']
})
export class SecondComponent implements OnInit {

  listContent : Content[];
  @Output() selector = new EventEmitter<Content>();

  constructor(private saveStudentService: SaveStudentService,
              private router: Router,
              private progress: ProgressIndicatorService) { 
    saveStudentService.getStudent()
    .then(result => this.listContent = result)
    .catch(this.handleError);
  }

  private handleError(errors: any): void {
    console.log("Terjadi error : "+errors);
  }

  selectSecond(r : Content){
    this.selector.emit(r);
    console.log("selected content is "+r.name);
  }

  ngOnInit() {
  }

}
