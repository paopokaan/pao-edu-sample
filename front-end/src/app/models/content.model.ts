export class Content{
    public constructor(
        public id: string,
        public name: string, 
        public year: number,
        public address: string
    ) {}
}