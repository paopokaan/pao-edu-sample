import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {

  face_image = '../assets/images/face.jpg';
  icon_1 = '../assets/images/icons/1.png';
  icon_9 = '../assets/images/icons/9.png';
  icon_setting = '../assets/images/icons/10.png';

  constructor() { }

  ngOnInit() {
  }

}
