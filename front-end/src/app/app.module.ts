import { BrowserModule } from '@angular/platform-browser';
import { NgModule, Component } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AccordionModule } from 'ngx-bootstrap';
import { ModalModule } from 'ngx-bootstrap';

import { AppComponent } from './app.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { NavigationbarComponent } from './navigationbar/navigationbar.component';

import { AboutComponent } from './about/about.component';
import { IndexComponent } from './index/index.component';
import { ContentModule } from './content/content.module';
import { TransactionModule } from './transaction/transaction.module';

import { ProgressIndicatorService } from './services/progress-indicator.service';
import { PagerService } from './services/pager.service';
import { SearchComponent } from './navigationbar/search/search.component';
import { FooterComponent } from './navigationbar/footer/footer.component';
import { LoginComponent } from './login/login.component';

import { AuthguardCheckLoginService } from './services/authguard-check-login.service';
import { AuthService } from './services/auth.service';

const routingApplication: Routes = [
  { path: 'about', component: AboutComponent, pathMatch: 'full', canActivate: [AuthguardCheckLoginService] },
  { path: 'login', component: LoginComponent },
  { path: 'content', redirectTo: '/content', pathMatch: 'full', canActivate: [AuthguardCheckLoginService] },
  { path: '**', component: IndexComponent }
];
@NgModule({
  declarations: [
    AppComponent,
    SidebarComponent,
    NavigationbarComponent,
    AboutComponent,
    IndexComponent,
    SearchComponent,
    FooterComponent,
    LoginComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule.forRoot(routingApplication),
    ModalModule.forRoot(),
    AccordionModule.forRoot(),
    ContentModule,
    TransactionModule
  ],
  providers: [ProgressIndicatorService, PagerService, AuthguardCheckLoginService, AuthService],
  bootstrap: [AppComponent]
})
export class AppModule { }
