import { Component, ViewChild, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import { ProgressIndicatorService } from './services/progress-indicator.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  exportAs: 'child'
})
export class AppComponent implements OnInit, OnDestroy {
  title = 'latihan pao-BK';
  notification: string;
  subscription: Subscription;

  face_image = '../assets/images/face.jpg';
  logo_starblack_image = '../assets/images/logo_star_black.png';
  logo_starmini_image = '../assets/images/logo_star_mini.jpg';


  @ViewChild('staticModal') staticModal;

  constructor(private progress: ProgressIndicatorService) {

  }

  ngOnInit() {
    this.subscription = this.progress.getStatus().subscribe(data => {
      this.notification = data;
      // if(data) {
      //     this.staticModal.show();
      // } else {
      //     this.staticModal.hide();
      // }
    });
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

}
