import { Injectable } from '@angular/core';

import { User } from './../models/user.model';

@Injectable()
export class AuthService {

  private currentUser: User;


  constructor() { }

  isLogin(): boolean {
    this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
    if (this.currentUser) {
      console.log('username : ' + this.currentUser.username);
      console.log('token : ' + this.currentUser.token);
    }
    return this.currentUser != null;
  }

  getCurrentUser() {
    if (this.isLogin) {
      return this.currentUser;
    }
    return null;
  }

  login(username: string, password: string) {
    this.currentUser = new User(username, password);
    localStorage.setItem('currentUser', JSON.stringify(this.currentUser));
  }

}
