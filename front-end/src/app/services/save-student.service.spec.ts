import { TestBed, inject } from '@angular/core/testing';

import { SaveStudentService } from './save-student.service';

describe('SaveStudentService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SaveStudentService]
    });
  });

  it('should be created', inject([SaveStudentService], (service: SaveStudentService) => {
    expect(service).toBeTruthy();
  }));
});
