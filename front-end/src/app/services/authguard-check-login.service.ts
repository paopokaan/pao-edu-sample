import { AuthService } from './auth.service';
import { CanActivate, Router } from '@angular/router';
import { Injectable } from '@angular/core';


@Injectable()
export class AuthguardCheckLoginService implements CanActivate {

  constructor(
    private authService: AuthService,
    private router: Router
  ) { }

  canActivate() {
    console.log('Sudah login ? ' + this.authService.isLogin());
    if (this.authService.isLogin()) {
      return true;
    }
    this.router.navigate(['/login']);
    return false;
  }

  canActivateChild() {
    return this.canActivate();
  }

}
