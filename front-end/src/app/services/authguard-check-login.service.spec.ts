import { TestBed, inject } from '@angular/core/testing';

import { AuthguardCheckLoginService } from './authguard-check-login.service';

describe('AuthguardCheckLoginService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AuthguardCheckLoginService]
    });
  });

  it('should be created', inject([AuthguardCheckLoginService], (service: AuthguardCheckLoginService) => {
    expect(service).toBeTruthy();
  }));
});
