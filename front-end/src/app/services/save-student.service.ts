import { Injectable } from '@angular/core';
import { Headers, Http, Response, RequestOptions } from '@angular/http';
import { Observable, BehaviorSubject, Subject } from 'rxjs/Rx';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

import { Content } from './../models/content.model';

@Injectable()
export class SaveStudentService {

  private serverUrl = 'api/student';

  private students: Observable<Content[]>;
  private _students: BehaviorSubject<Content[]>;
  private datastore: {students: Content[]};
  private _changeDetector: Subject<Boolean> = new Subject<Boolean>();

  constructor(private http: Http) {
    this.datastore = { students: [] };
    this._students = <BehaviorSubject<Content[]>>new BehaviorSubject([]);
    this.students = this._students.asObservable();
  }

  getStudent(): Promise<any> {
    return this.http.get(this.serverUrl)
      .toPromise()
      .then(hasil => hasil.json() as Content[])
      .catch(this.handleError);
  }

  getStudent2(): Observable<Content[]> {
    return this.http.get(this.serverUrl)
      .map(result => result.json() as Content[])
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
  }

  saveStudent(student: Content): Promise<void> {
    return this.http.post(this.serverUrl, student)
      .toPromise()
      .then(() => { console.log('Sukses menyimpan data'); })
      .catch(error => console.log('Error : ' + error));
  }

  saveStudent2(student: Content): Observable<Content[]> {
    const bodyString = JSON.stringify(student);
    const headers = new Headers({ 'Content-Type': 'application/json' });
    const options = new RequestOptions({ headers: headers }); // Create a request option
    return this.http.post(this.serverUrl, student, options)
      .map((res: Response) => res.json())
      .catch((error: any) => Observable.throw('Server error'));
  }

  saveStudent3(student: Content) {
    const headers = new Headers({ 'Content-Type': 'application/json' });
    const options = new RequestOptions({ headers: headers }); // Create a request option
    return this.http.post(this.serverUrl, student, options)
      .map((response: Response) => response.json())
      .subscribe(data => {
        if (typeof data.error !== 'undefined') {
          console.log('data undefine');

          return;
        }

        this.datastore.students.push(student);
        this._students.next(Object.assign({}, this.datastore).students);
        this._changeDetector.next(true);
      }, error => console.log('failed save data'));
  }

  updateStudent(student: Content, id): Promise<void> {
    return this.http.put(this.serverUrl + '/' + id, student)
      .toPromise()
      .then(() => { console.log('sukses update data'); })
      .catch(error => console.log('Error : ' + error));
  }

  updateStudent2(student: Content, id: string): Observable<Content[]> {
    const bodyString = JSON.stringify(student);
    const headers = new Headers({ 'Content-Type': 'application/json' });
    const options = new RequestOptions({ headers: headers }); // Create a request option
    return this.http.put(this.serverUrl + '/' + id, student, options)
      .map((res: Response) => res)
      .catch((error: any) => Observable.throw('failed update'));
  }

  deleteStudent(id): Promise<void> {
    return this.http.delete(this.serverUrl + '/' + id)
      .toPromise()
      .then(() => { console.log('success delete data'); })
      .catch(error => console.log('Error : ' + error));
  }

  deleteStudent2(id: string): Observable<Content[]> {
    return this.http.delete(this.serverUrl + '/' + id)
      .map((res: Response) => res)
      .catch((error: any) => Observable.throw('Server error'));
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error); // for demo purposes only
    return Promise.reject(error.message || error);
  }

}
