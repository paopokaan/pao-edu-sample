# Pao-Edu #

Pao edu is simple project student data and report using spring boot and angularjs 2.

### How do I run? ###

* open tab terminal change directory ${project_path}/back-end: mvn clean spring-boot:run
* open other tab terminal change directory $ {project_path}/front-end: ng serve --proxy-config proxy.conf.json

### How do I get set up? ###

* this sample using in memory database, mean that if restart backend server. all data would reset. 
* if user want to change to mysql database, change content of application.properties in path {project_path}/back-end/src/main/resources to application.properties.mysql in same path.
