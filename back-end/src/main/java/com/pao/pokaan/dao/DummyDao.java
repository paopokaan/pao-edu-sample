package com.pao.pokaan.dao;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.pao.pokaan.entity.Dummy;

public interface DummyDao extends PagingAndSortingRepository<Dummy, String> {

}
