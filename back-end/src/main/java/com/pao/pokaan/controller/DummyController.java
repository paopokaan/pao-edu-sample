package com.pao.pokaan.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.pao.pokaan.dao.DummyDao;
import com.pao.pokaan.entity.Dummy;

@RestController
@RequestMapping("dummy")
public class DummyController {

	@Autowired
	private DummyDao dummyDao;
	
	@RequestMapping(method=RequestMethod.GET)
	public Iterable<Dummy> getAll(Pageable p){
		return dummyDao.findAll(p);
	}
}
