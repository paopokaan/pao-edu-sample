package com.pao.pokaan.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.pao.pokaan.dao.StudentDao;
import com.pao.pokaan.entity.Student;

@RestController
@RequestMapping(value="student")
public class StudentController {

	@Autowired
	private StudentDao studentDao;
	
	@RequestMapping(method = RequestMethod.GET)
    public Iterable<Student> getAll(Pageable p){
        return studentDao.findAll(p);
    }
	
	@RequestMapping(method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    public Student save(@RequestBody Student s){
        studentDao.save(s);
        return s;
    }
	
	@RequestMapping(value = "/{id}", method = RequestMethod.PUT)
	@ResponseStatus(HttpStatus.OK)
	public Student update(@PathVariable("id") String id, @RequestBody Student s){	
		s.setId(id);
		studentDao.save(s);
		return s;
	}
	
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	@ResponseStatus(HttpStatus.OK)
	public String delete(@PathVariable("id") String id){
		Student s = studentDao.findOne(id);
		studentDao.delete(s);
		return id;
	}
	
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	@ResponseStatus(HttpStatus.OK)
	public Student getById(@PathVariable("id") String id){
		return studentDao.findOne(id);
	}
	
}
